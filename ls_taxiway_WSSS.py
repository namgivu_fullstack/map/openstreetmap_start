from pathlib import Path
from service.openstreetmap_service import ls_taxiway

'fetch data from origin source';    taxiway_d__list = ls_taxiway(icao='WSSS')

'rewrite taxiway_d__list to combine nodes of all ref=W eg'
d={}
for taxiwayname,l in taxiway_d__list.items():
  d[taxiwayname]=[]
  for i in l:
    d[taxiwayname] += i['nodes']

taxiway_d__list=d

# import json; taxiway_list_s = json.dumps(taxiway_list, indent=2, default=str)
'';            taxiway_list_s = str(taxiway_d__list)

(Path(__file__).parent/f'zo_{Path(__file__).stem}.py').write_text(f'''
from decimal import Decimal
taxiway_d__list = {taxiway_list_s}
'''.strip()+'\n')
